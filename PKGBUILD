# AArch64 Apple Silicon
# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Hector Martin <marcan@marcan.st>

pkgname=calamares-apple-silicon-configs
pkgver=20220712
pkgrel=3
pkgdesc='Apple Silicon Calamares setup configs'
arch=('any')
url='http://asahilinux.org'
license=('MIT')
depends=('calamares' 'manjaro-kde-settings')
makedepends=('git')
source=("asahi-calamares-configs-${pkgver}.tar.gz::https://github.com/AsahiLinux/asahi-calamares-configs/archive/refs/tags/${pkgver}.tar.gz"
        git+https://github.com/Strit/calamares-arm-oem.git)
sha256sums=('27f79ac4e03848fecb6535d1301ef3e2757271fcfaa08c0eeb2296e3835b052d'
            'SKIP')
b2sums=('87d95405b7438ec7a9f9f19bc391979c75012ed7716c29bbfea9f9fff76fcb06ae419e01afe43d74215a0357bf1969963dbbc56bf3c5b6156a68f57a3af5ec23'
        'SKIP')

prepare() {
  cd "${srcdir}/asahi-calamares-configs-${pkgver}"
  sed -i -e 's|$(PREFIX)/share/calamares/|/etc/calamares/|g' Makefile
  sed -i -e 's|asahi|manjaro|g' calamares/settings.conf
  sed -i -e 's|Next/contents/images/5120x2880.jpg|Bamboo/contents/images/5120x2880.png|g' \
      "${srcdir}/asahi-calamares-configs-${pkgver}/bin/first-time-setup.sh"
  rm -rf calamares/branding
}

package() {
  cd "${srcdir}/asahi-calamares-configs-${pkgver}"
  make PREFIX=/usr DESTDIR=${pkgdir} install

  install -Dm644 "$srcdir/asahi-calamares-configs-${pkgver}/LICENSE" \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  
  echo "---" > "${pkgdir}/etc/calamares/modules/finished.conf"
  echo "restartNowEnabled: false" >> "${pkgdir}/etc/calamares/modules/finished.conf"
  echo "restartNowChecked: false" >> "${pkgdir}/etc/calamares/modules/finished.conf"
  echo "restartNowCommand: \"\"" >> "${pkgdir}/etc/calamares/modules/finished.conf"

  cd "${srcdir}/calamares-arm-oem"
  
  install -d "${pkgdir}/usr/share/calamares/branding/manjaro"
    cp -a manjaro/* "${pkgdir}/usr/share/calamares/branding/manjaro/"  
}
